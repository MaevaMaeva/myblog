var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Blog NodeJS' });
});

/*get page creationarticle*/
router.get('/creationarticle', (req, res) => {
    res.render('creationarticle');
});

module.exports = router;
